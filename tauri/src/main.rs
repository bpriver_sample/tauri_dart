#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            greet,
            simple_command,
            super_command,
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

#[tauri::command]
fn greet(name: &str) -> String {
    format!("Hello, {}!", name)
}

#[tauri::command]
fn simple_command() {
    println!("excute simple command.");
}

#[tauri::command]
fn super_command(value_a: &str, value_b: &str) -> String {
    format!("inputed {} & {}", value_a, value_b)
}