
import 'package:js/js.dart';

// JavaScript における Promise 型は promiseToFuture を使って Promise 型から Future 型へと、後で変換する必要がある.
// Dart では Promise 型は存在しないため この段階では Object 型として定義しておく。
@JS('invokeWrapper')
external Object invoke(TauriCommandArgument tauriCommandArgument);

@JS('')
@anonymous
abstract class TauriCommandArgument {
    String get commandName;
}

// JavaScript における Object 型 に対応するには 名前付き引数がある constructor を定義した class を定義しなければならい.
@JS('')
@anonymous
class GreetCommandArgument
    extends
        TauriCommandArgument
{
    external factory GreetCommandArgument({String commandName, String name});
    external String get commandName;
    external String get name;
}

@JS('')
@anonymous
class SimpleCommandArgument
    extends
        TauriCommandArgument
{
    external factory SimpleCommandArgument({String commandName});
    external String get commandName;
}

// rust では value_a という snake case 記法だが、何故かこっちでは camel case 記法じゃないと error になってしまう.
@JS('')
@anonymous
class SuperCommandArgument
    extends
        TauriCommandArgument
{
    external factory SuperCommandArgument({String commandName, String valueA, String valueB});
    external String get commandName;
    external String get valueA;
    external String get valueB;
}