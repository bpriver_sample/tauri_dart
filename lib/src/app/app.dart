
import 'package:ngdart/angular.dart';

import 'app.template.dart' as temp;

import '../invoke_button/invoke_button.dart';

@Component(
    selector: 'app',
    template: '<invoke-button></invoke-button>',
    directives: [
        InvokeButton,
    ],
)
class App {

    static final template = temp.AppNgFactory;

}