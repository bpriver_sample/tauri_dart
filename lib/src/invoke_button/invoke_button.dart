
import 'dart:html';

import 'package:ngdart/angular.dart';
import 'package:tauri_dart/src/invoke/invoke.dart';

import 'invoke_button.template.dart' as temp;

@Component(
    selector: 'invoke-button',
    template: '',
)
class InvokeButton
    implements
        OnInit
{

    static final template = temp.InvokeButtonNgFactory;

    InvokeButton(this.el);
    final HtmlElement el;
    
    @HostListener('click')
    void excute() async {
        
        // promiseToFuture を使って Object 型から Future 型へと変換する.
        promiseToFuture<String>(
            invoke(GreetCommandArgument(commandName: 'greet', name: 'arakawa'))
        ).then(
            (value) => window.console.log(value)
        );

        promiseToFuture<void>(
            invoke(SimpleCommandArgument(commandName: 'simple_command'))
        ).then(
            (value) => window.console.log('excuted simple_command')
        );

        promiseToFuture<String>(
            invoke(SuperCommandArgument(commandName: 'super_command', valueA: 'aaaaa', valueB: 'bbbbb'))
        ).then(
            (value) => window.console.log(value)
        );

    }
    
    @override
    void ngOnInit() {
        el.text = 'excute invoke';
        el.style.borderColor = 'black';
        el.style.borderStyle = 'solid';
        el.style.borderWidth = '1px';
        el.style.cursor = 'pointer';
    }

}