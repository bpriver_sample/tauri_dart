# tauri_dart

## 開発用 command
cargo tauri dev

## ホットリロードがバグったときの対処法
【Chrome】開発者ツールでキャッシュを無効化する | KAZUPON研究室: "https://kazupon.org/chrome-cache-disable/"
WebView2 - キャッシュ等のユーザーデータの保存フォルダを指定する｜プログラムでネットサーフィン: "https://web.biz-prog.net/praxis/webview/cache.html"

### WebView2
開発者ツールを開き、ネットワーク - キャッシュを無効にする の項目がある。
hot reload が効かなくなった場合
1. これにチェックをいれ Ctrl + R キーを押し更新し、ホットリロードが効くかどうかを試す。
2. これでも効かない場合、cargo tauri dev コマンドを停止して、もう一回 cargo tauri dev コマンドを開始して 1 の手順を繰り返す。
ホットリロードが効くようになるまで 1 と 2 を繰り返せはなおる。
ホットリロードが効かなくなるのはブラウザ内のキャッシュが原因で このキャッシュを削除できれば正常にホットリロードするようになるのだが、
WebView2 の簡単なキャッシュの削除の仕方がわからないので、現在このような手順が一番楽と思われる。
この ホットリロードが効かなくなる現象は、Chrome, Edge, Brave などでも同様に起きる。
きっかけは、dart run build_runner serve --live-reload ではなく、
「dart run build_runner serve」と、コマンドを入力してしまうことにある。
原因はよくわからないが、これを実行してしまった後に、dart run build_runner serve --live-reload を実行してもホットリロードが効かなくなってしまう。
「dart run build_runner serve」を行った期間のキャッシュを削除することで治る。

## dart run build_runner serve --live-reload でエラー
### message
[SEVERE] Failed to precompile build script .dart_tool/build/entrypoint/build.dart.
This is likely caused by a misconfigured builder definition.
### 対処法
「dart pub upgrade」コマンドを試す。
理由はよくわからない。
"https://minpro.net/failed-to-precompile-build_runner"
"https://github.com/dart-lang/build/issues/1676"
"https://dart.dev/tools/pub/cmd/pub-upgrade"
